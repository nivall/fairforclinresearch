# Variable description

Data collection was performed within different French tertiary care centers. Data names were collected with french word. Translation of the terms and coding is described below.x

## Common variables

- `CENTRE`: identification of the center in which the patient was included 
- `NUM`: number of inclusion	
- `Identifiant`: identifier	

## clinical.csv, demographic data

- `SEXE`: gender, "2-Feminin" = Female, "1-Masculin" = Male
- `AGE`: age at inclusion	
- `HEMOPATHIE`: type of plasmocyte disease : myeloma or plasmocytic leukemia	
- `TYPE`: type of immunoglobulin involved (CL = "light chains")
- `ISS`: [international staging system](https://www.myeloma.org/international-staging-system-iss-reivised-iss-r-iss)
- `R.ISS`: [revised international staging system](https://www.myeloma.org/international-staging-system-iss-reivised-iss-r-iss)
- `CYTO`: cytogenetic risk, high risk = (del(17p), t(4,14), t(14,18)), standard risk = normal or non-high risk abnormalities
- `EXTRAMED`: extramedullary localisation (O = no, 1 = yes)
- `DIAGMYELOME`: period of disease diagnosis	
- `ANNEEDG`: year of diagnosis of the disease	
- `DATE1ERELIGNE`: date first line therapy	
- `ANNEE1ereligne`: year first line treatment	
- `LIGNESANT`: number of previous line of treatment	
- `IMID`: previous IMID use (0 = not exposed, 1 = exposed, 2 = refractory)
- `ANTIPROTEASOME`: previous proteasome inhibitor use (0 = not exposed, 1 = exposed, 2 = refractory)
- `DARATUMUMAB.ISATUXIMAB`: previous anti-CD38 treatment (0 = not exposed, 1 = exposed, 2 = refractory)
- `CART`: previous CAR-T cells infusion (O = no, 1 = yes)
- `AUTOGREFFE`: previous hematopoietic stem cell transplant (HSCT, 0 = no, 1 = autologous, 2 = allogeneic)
- `NOMBRESAUTOGREFFE`: number of previous autologous stem cell transplant
- `DATEAUTOGREFFE1`: first auto-HSCT period
- `ANNEEAUTOGREFFE1`: first auto-HSCT year
- `DATEAUTOGREFFE2`: second auto-HSCT period
- `ANNEEAUTOGREFFE2`: second auto-HSCT year	
- `DATEAUTOGREFFE3`: third auto-HSCT period
- `ANNEEAUTOGREFFE3`: third auto-HSCT year
- `ALLOGREFFE`: previous allogeneic HSCT
- `AUTRESIMMUNODEP`: other risk of immunosuppression (0 = no, 1 = solid cancer)
- `CHARLSON`: [Charlson index](https://healthcaredelivery.cancer.gov/seermedicare/considerations/comorbidity.html)
- `BISPE`: type of bispecific (bispe) monoclonal antibodies (1 = teclistamab, 2 = elranatamab, 3 = talquetamab
- `DEBUT`: first bispe administration date
- `TTTASSOCIE`: treatment associated with bispe	(0 = none, 1 = daratumumab+pomalidomide+dexamethasone, 2 = lenalidomide, 3 = Cetrilimab, 4 = dexamethasone alone, 5 = Carfilzomib, 7 = Daratumumab+lenalidomide, 8 = daratumumab alone)
- `CRS`: cytokine release syndrome (0 = no, 1 = yes)
- `GRADECRS`: maximum CRS grade	
- `ICANS`: Immune effector cell-associated neurotoxicity syndrome (0 = no, 1 = yes)
- `CTC.ICANS.CRS`: use of corticosteroids for CRS or ICANS treatment	
- `TOCI`: use of tocilizumab
- `NOMBRES.SEMAINES.NEUTROPENIE<500.mm3`: number of weeks of neutropenia grade 4 defined by absolute neutrophil count < 0.5e9/L
- `TOTAL.INF`: number of infectious (not used for the analyses)
- `DDN`: date of last follow up	
- `Statut.DDN`: status at last follow up (1 = alive, 2 = deceased)
- `Statut.hemopathie.DDN`: status of the disease at last follow up (1 = ongoing bispe treatment, 2 = other treatment line, 3 = interrupted treatments)

## infectious.csv, infectious events data

- `PROPHYLAXIEBACTERIO`: ongoing prophylaxis against bacterial infectious (0 = no, 1 = yes)
- `PROPHYLAXIE.STX.ATV`: ongoing oral anti-pneumocystis and anti-toxoplasmosis prophylaxis (0 = no, 1 = yes)
- `PROPHYLAXIE.PENTA`: ongoing inhale anti-pneumocystis and anti-toxoplasmosis prophylaxis (0 = no, 1 = yes)
- `PROHYLAXIE.HSV`: ongoing anti-HSV prophylaxis (0 = no, 1 = yes)
- `PROPHYLAXIE.CMV`: ongoing anti-CMV prophylaxie (0 = no, 1 = yes)	
- `PROPHYLAXIE.ANTIFONGIQUE`: ongoing antifungal prophylaxis (0 = no, 1 = yes)
- `PREVENAR.PNEUMOVAX`: updated anti-pneumococcal vaccines (0 = no, 1 = yes)
- `SUPP.IGG`: recent polyclonal immunoglobulin supplementation (0 = no, 1 = yes)	
- `CYCLES`: number of cycles of treatment
- `REPONSE`: [response to treatment at time of infectious events](https://www.myeloma.org/resource-library/international-myeloma-working-group-imwg-uniform-response-criteria-multiple)
- `DOSE.CTC`: corticosteroid dose 	
- `GB.mm3`: absolute leucocyte count e9/L	
- `PNN.mm3`: absolute neutrophil count e9/L	
- `GCSF`: G-CSF administration 	
- `Monoc.mm3`: absolute monocyte count e9/L	
- `Lc.mm3`: absolute lymphote count e9/L	
- `IGTOTALES`: immunoglobuine (g/L) after substracting monoclonal component	
- `CD4`: absolute CD4+ T cells count e9/L	
- `CD8`: absolute CD8+ T cells count e9/L	
- `INFECTION.POST.BISPE`: infectious event (0 = no, 1 = yes)
- `DATE`: date infectious event	
- `REA.USC.USI`: treatment in intensive care unit (0 = no, 1 = yes)
- `HOSPIT`: hospitalization requiered (0 = no, 1 = yes)
- `DUREE.HOSPI`: hospitalisation duration (days)
- `TYPEINFECTION`: site of infection (1 = systemic, 2 = respiratory lower, 3 = respiratory upper, 4 = central nervous system, 5 = digestive, 6 = soft tissues, 7 = Urine, or not documented)
- `TYPEPATHOGENE`: type of pathogen (1 = bacterial, 2 = viral, 3 = parasitis, 4 = fungal)
- `IDENT.PATHOGENE.1`: details on pathogen 
- `IDENT.PATHOGENE.2`: details on pathogen if 2 documented
- `grade.infection`: grade infectious event according to [CTCAE v5](https://ctep.cancer.gov/protocoldevelopment/electronic_applications/ctc.htm)
- `CATEGORIE.INFECTION`: infection category (1 = clinically documented, 2 = microbiologically diagnosed, 3 = not documented)
- `EVOLUTION`: outcome (1 = cured without impact, 2 = cured with clinically significant impact, 3 = persisting infection, 4 = death related to infectious event, 5 = death not related to infectious event, 6 = ongoing infectious event treatment)
- `PROPHYLAXIE.SECONDAIRE`: secondary prophylaxis initiated (0 = no, 1 = yes)
- `IMPACT`: impact on treatment (0 = no impact, 1 = disease treatment stopped, 2 = delayed treatment, 3 = dose adjustment)
