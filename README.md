# FAIR principles applied to clinical research, a proof of concept

This repository stores data and computing environment as a proof of concept of FAIR principles applied to clinical research. Data are composed of a sample of 100 patients included in a retrospective study which evaluated infectious events in multiple myeloma.

The structure of the repository is described below.

To compute the analyses reports in `HTML` format, run the `compile-all.sh` file in a `shell`.

## Raw data `data`

This directory stores raw data used for the analyses in `CSV` format : 

* `clinical.csv` describes clinical variables from each included patients
* `infections.csv` describes all infectious events from each included patients

## Computing environment configuration (with GNU-Guix) `guixconfig`

### Content of the directory

This directory stores `SCM` files which define the fixed timepoint `channels.scm` used to build the environment with the required packages `manifest.scm` and `my-pkgs.scm`.

### Regarding computing environment reproducibility and transparency

Computing environment is defined as the stack of software on which a computer perform analyses. To make sure this stack is transparent and reproducible between computer and in the future, we used [Guix](https://guix.gnu.org/). Installation and tutorial can be read on [Guix-HPC website](https://hpc.guix.info/). Guix can be used in fondamental science to allow transparency in data processing and analyses procesure as reported by [Vallet et al. Sci Data. 2022](https://doi.org/10.1038/s41597-022-01720-9) and the following [gitlab repository](https://gitlab.com/nivall/guixreprodsci).

### How Guix was used in this project

Please refere to the `guixconfig` directory to read details. 
To make it short, reproducibility of analyses are demonstrated by using a computing environment packaged in a Docker container [nivallettours:compute-v1](https://hub.docker.com/layers/nivallettours/mbispid/compute-v1/images/sha256-3450cb300684a23c7334153936ec3048a6f4b8bac67bcb64687fcac203ecddf3?context=explore) by Guix. It was deployed on GitLabCI and source code of analyses were used to compile `.Rmd` files.

The results are hosted here:

* [Main characteristics of patients](https://nivall.gitlab.io/fairforclinresearch/mainCharacteristics.html)
* [Characteristics of infectious events](https://nivall.gitlab.io/fairforclinresearch/mainInfectionsCharact.html)
* [Cumulative incidence analyses](https://nivall.gitlab.io/fairforclinresearch/firstInfection.html)

## Metadata `metadata`

This directory stores files used to define or complete variable description

## Markdown files to compute the analyses report `notebooks`

This directory stores three reports analyses :

* `firstInfection.Rmd` : first infectious events and related associated variable analyses
* `mainCharacteristics.Rmd` : included patients characteristics
* `mainInfectionsCharact.Rmd` : all infectious events characteristics

## Source code written in `R` `scripts`

This directory stores scripts that define list of variable or perform datamanagement :

* `cumincidence.R` : datamanagement to perform cumulative incidence analyses
* `importClinical.R` : datamanagement to import clinical data
* `importInfec.R` : datamanagement to import infectious events data
* `variableList.R` : defines lists of variables used for analyses 

# Long term archive of the project

To allow long term archive and reproducibility, this project was saved on [Software Heritage](https://archive.softwareheritage.org/swh:1:dir:382fae872698304db7aa1b2789cab437bf452e98)
