(specifications->manifest
 (list
  ;; upstream packages
  "r"
  "r-tidyverse"
  "r-ggplot2"
  "r-cowplot"
  "r-rstatix"
  "r-rcolorbrewer"
  "r-cmprsk"
  "r-prodlim"
  "r-survminer"
  "r-viridis"
  "r-rmarkdown" ; render .Rmd files

  ;; defined in my-pkgs/my-pkgs.scm
  "r-gtsummary"
  "r-gt"
  "r-riskregression"

  ;; Only for Docker GitLabCI
  "coreutils"
  "bash"
  "xz"
  "git"
  "nss-certs"
  ))
